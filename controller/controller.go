package controller

import (
	"net/http"
	"server/service"

	"github.com/gin-gonic/gin"
)

func Control(r *gin.Engine) {
	r.POST("/list/douban", func(c *gin.Context) {
		var page service.Page
		c.BindJSON(&page)

		list := service.QueryDoubanList(page)

		c.JSON(http.StatusOK, gin.H{
			"data": list,
		})
	})
}
