package db

import (
	"context"
	"database/sql"
	"flag"
	"log"
	"time"

	"github.com/didi/gendry/manager"
	_ "github.com/go-sql-driver/mysql"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	sqlAdr      = "rm-bp1h450hxe70ql6q5mo.mysql.rds.aliyuncs.com"
	TableDouban = "db"
)

var host = flag.String("h", "localhost", "mongodb host")
var mongoDb *mongo.Client
var mySql *sql.DB

func ConnectMongo() *mongo.Client {
	if mongoDb != nil {
		return mongoDb
	}
	var err error
	mongoDb, err = connectMongo()
	if err != nil {
		log.Println("Mongo connect error:", err)
	}
	return mongoDb
}

func ConnectSQL() *sql.DB {
	if mySql != nil {
		return mySql
	}
	var err error
	mySql, err = connectSQL()
	if err != nil {
		log.Println("SQL connect error:", err)
	}
	return mySql
}

func connectMongo() (*mongo.Client, error) {
	flag.Parse()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	credential := options.Credential{
		Username: "root",
		Password: "112233",
	}
	return mongo.Connect(ctx, options.Client().ApplyURI("mongodb://"+*host+":20000").SetAuth(credential))
}

func connectSQL() (*sql.DB, error) {
	db, err := manager.New("douban", "root", "LZP199508*", sqlAdr).Set(
		manager.SetCharset("utf8"),
		manager.SetAllowCleartextPasswords(true),
		manager.SetInterpolateParams(true),
		manager.SetTimeout(5*time.Second),
		manager.SetReadTimeout(5*time.Second),
	).Port(3306).Open(true)

	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}

	return db, err
}
