package service

import (
	"log"
	"server/crawl"
	"server/db"
	sdb "server/db"

	"github.com/didi/gendry/builder"
	"github.com/didi/gendry/scanner"
)

type Page struct {
	PageNo   int `json:"pageNo" binding:"required"`
	PageSize int `json:"pageSize" binding:"required"`
}

func QueryDoubanList(page Page) []crawl.DoubanData {
	db := db.ConnectSQL()

	where := map[string]interface{}{
		"_limit": []uint{uint((page.PageNo - 1) * page.PageSize), uint(page.PageNo * page.PageSize)},
	}

	cond, vals, err := builder.BuildSelect(sdb.TableDouban, where, []string{})
	if err != nil {
		log.Panicln("Build Select error:", err)
	}
	rows, err := db.Query(cond, vals...)
	if err != nil {
		log.Panicln("Query error:", err)
	}
	defer rows.Close()
	var list []crawl.DoubanData
	scanner.Scan(rows, &list)

	return list
}
