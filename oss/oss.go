package oss

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"path/filepath"
	"time"

	"github.com/tencentyun/cos-go-sdk-v5"
)

var client *cos.Client

var OSSAdr = "https://image-1304326448.cos.ap-guangzhou.myqcloud.com"

func init() {
	u, _ := url.Parse(OSSAdr)
	b := &cos.BaseURL{BucketURL: u}
	client = cos.NewClient(b, &http.Client{
		//设置超时时间
		Timeout: 100 * time.Second,
		Transport: &cos.AuthorizationTransport{
			//如实填写账号和密钥，也可以设置为环境变量
			SecretID:  "AKIDFMgAYi6qiPvIX1G2Zy87scYMxLXWjaGB",
			SecretKey: "yoPmxs328FI9nDmZwQjvsdfV1KUcYorh",
		},
	})
}

func IsExist(name string) bool {
	_, err := client.Object.Head(context.Background(), name, nil)

	return err == nil
}

func UploadDoubanCover(coverUrl string) string {
	resp, err := http.Get(coverUrl)
	if err != nil {
		log.Println("Get coverUrl error:", err)
	}
	defer resp.Body.Close()

	return Upload(filepath.Base(coverUrl), resp.Body)
}

func Upload(name string, body io.Reader) string {
	_, err := client.Object.Put(context.Background(), name, body, nil)
	if err != nil {
		return ""
	} else {
		return fmt.Sprintf("%s/%s", OSSAdr, name)
	}
}
