package crawl

import (
	"database/sql"
	"fmt"
	"log"
	"regexp"
	"server/db"
	sdb "server/db"
	"server/oss"
	"strconv"
	"strings"
	"time"

	"github.com/didi/gendry/builder"
	"github.com/didi/gendry/scanner"
	"github.com/gocolly/colly/v2"
)

type DoubanData struct {
	Id      int     `ddb:"id"`
	Url     string  `ddb:"url"`
	Name    string  `ddb:"name"`
	Author  string  `ddb:"author"`
	Score   float32 `ddb:"score"`
	Library string  `ddb:"library"`
	Cover   string  `ddb:"cover"`
}

var visited = map[string]DoubanData{}

func insert(db *sql.DB, data DoubanData) {
	log.Printf("%s :%v\n", data.Library, data)

	res, _ := scanner.Map(data, "ddb")
	cond, vals, err := builder.BuildInsert(sdb.TableDouban, []map[string]interface{}{res})

	if err != nil {
		panic(err)
	}

	_, err = db.Exec(cond, vals...)

	if err != nil {
		log.Fatalf("Insert error %s: %v", data.Library, err)
	} else {
		log.Printf("Insert success, id: %v, name: %s", data.Id, data.Name)
	}
}

func queryById(db *sql.DB, id int) DoubanData {
	where := map[string]interface{}{
		"id": id,
	}
	cond, vals, _ := builder.BuildSelect(sdb.TableDouban, where, []string{"id"})

	rows, err := db.Query(cond, vals...)

	if err != nil {
		log.Println("Query error: ", err)
	}
	defer rows.Close()
	var res DoubanData
	scanner.Scan(rows, &res)

	return res
}

func updateKeyOfString(db *sql.DB, data DoubanData, key string) { // 更新值为string的某个key
	where := map[string]interface{}{
		"id": data.Id,
	}
	update := map[string]interface{}{}
	switch key {
	case "Cover":
		update["cover"] = data.Cover
	case "Url":
		update["url"] = data.Url
	}

	cond, vals, _ := builder.BuildUpdate(sdb.TableDouban, where, update)
	_, err := db.Exec(cond, vals...)
	if err != nil {
		panic(err)
	}
}

func Run() {
	sql := db.ConnectSQL()

	bookCh := make(chan DoubanData, 1)
	movieCh := make(chan DoubanData, 1)

	go NewDoubanCrawler("https://book.douban.com/", bookCh)
	go NewDoubanCrawler("https://movie.douban.com/", movieCh)

	for {
		select {
		case data := <-bookCh:
			data.Library = "Book"
			res := queryById(sql, data.Id)

			if res.Id != 0 {
				if res.Cover != data.Cover {
					updateKeyOfString(sql, data, "Cover")
				}
				if res.Url != data.Url {
					updateKeyOfString(sql, data, "Cover")
				}
			} else {
				insert(sql, data)

			}
		case data := <-movieCh:
			data.Library = "Movie"
			res := queryById(sql, data.Id)

			if res.Id != 0 {
				if res.Cover != data.Cover {
					updateKeyOfString(sql, data, "Cover")
				}
				if res.Url != data.Url {
					updateKeyOfString(sql, data, "Url")
				}
			} else {
				insert(sql, data)

			}
		}
	}
}

func NewDoubanCrawler(url string, ch chan<- DoubanData) {
	c := colly.NewCollector(
		colly.MaxDepth(2),
	)

	subjectRegExp, _ := regexp.Compile(url + `subject\/(\d+)\/?$`)

	c.OnHTML("#wrapper", func(h *colly.HTMLElement) {
		requestUrl := fmt.Sprintf("%v", h.Request.URL)

		data := visited[requestUrl]

		submat := subjectRegExp.FindSubmatch([]byte(requestUrl))
		if len(submat) >= 1 {
			data.Id, _ = strconv.Atoi(string(submat[1]))
		} else {
			return
		}
		name := h.ChildText(`span[property="v:itemreviewed"]`)
		score := h.ChildText(`strong[property="v:average"]`)
		author := h.ChildText(`#info span span ~ a`)
		cover := h.ChildAttr(`#mainpic img`, "src")

		data.Cover = oss.UploadDoubanCover(cover)

		data.Url = requestUrl
		data.Name = name
		data.Author = author
		f, _ := strconv.ParseFloat(strings.Trim(score, " "), 32)
		data.Score = float32(f)

		if len(data.Name) > 0 {
			ch <- data
		}
	})

	c.OnHTML("a[href]", func(h *colly.HTMLElement) {
		link := h.Attr("href")

		if _, has := visited[link]; has {
			return
		}

		if !subjectRegExp.Match([]byte(link)) {
			return
		}
		// 因为大多数网站有反爬虫策略
		// 所以爬虫逻辑中应该有 sleep 逻辑以避免被封杀
		time.Sleep(time.Second * 10)

		visited[link] = DoubanData{}

		time.Sleep(time.Microsecond * 2)
		c.Visit(h.Request.AbsoluteURL(link))
	})

	err := c.Visit(url)
	if err != nil {
		log.Fatalln(err)
	}
}
