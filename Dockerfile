FROM alpine

RUN mkdir /app
RUN apk add --no-cache libc6-compat
WORKDIR /app

EXPOSE 50051

CMD ["/app/main", "-h", "1.117.62.60"]