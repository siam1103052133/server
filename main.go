package main

import (
	"fmt"
	"server/controller"

	"github.com/gin-gonic/gin"
)

const (
	port = "5000"
)

func main() {

	// go crawl.Run() // Start crawl system

	router := gin.Default()

	controller.Control(router)

	router.Run(fmt.Sprintf(":%s", port))
}
